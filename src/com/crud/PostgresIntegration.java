package com.crud;

import java.sql.*;

public class PostgresIntegration {

    private static Connection conn = null;
    private Statement stmt = null;
    private ResultSet rs = null;

    private static String url = "jdbc:postgresql://localhost:5432";
    private static final String user = "postgres";
    private static final String pass = "postgres";
    private static String database = "Employee";
    private static final String connURL = url + "/" + database;

    public static void openPGConnection() {
        try {
            conn = DriverManager.getConnection(connURL, user, pass);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Connection connectPostgreSQL() throws SQLException {
        PostgresIntegration.openPGConnection();
        return conn;
    }

    public ResultSet getResultSetFromTable(String strQuery) throws SQLException {
        if (conn != null) {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(strQuery);
        }
        return rs;
    }

    /*public int updateORdeleteQuery(String sqlQuery) throws SQLException {
        int isAffectedCount = 0;
        if(conn != null) {
            isAffectedCount = conn.createStatement().executeUpdate(sqlQuery);
        }
        return isAffectedCount;
    }*/

    public void executeQuery(String sqlQuery) throws SQLException {
        if (conn == null) {
            connectPostgreSQL();
        }
        conn.createStatement().execute(sqlQuery);
    }

    public void closePostgresConnection() {
        try {
            if (stmt != null && !stmt.isClosed()) {
                stmt.close();
            }
            if (rs != null && !rs.isClosed()) {
                rs.close();
            }
            if (conn != null && !conn.isClosed()) {
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
