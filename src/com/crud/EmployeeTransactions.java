package com.crud;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeTransactions {

    private final PostgresIntegration postgresIntegration;

    public EmployeeTransactions() throws SQLException {
        postgresIntegration = new PostgresIntegration();
        connectPostgres();
    }

    public void connectPostgres() throws SQLException {
        Connection conn = postgresIntegration.connectPostgreSQL();
        if (conn == null) {
            System.out.println("Postgres is not connected...");
        }
    }

    public ResultSet getTransactionRecord(String strQry) throws SQLException {
        ResultSet tranRS = postgresIntegration.getResultSetFromTable(strQry);
        return tranRS;
    }

    public void executeQuery(String strQry) throws SQLException {
        postgresIntegration.executeQuery(strQry);
    }

    public void closeSQLConn() {
        postgresIntegration.closePostgresConnection();
    }
}
