package com.crud;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

public class crudform {

    private JPanel mainpanel;
    private JTextField txtEmpFirstName;
    private JTextField txtEmpLastName;
    private JTextField txtEmpDepartment;
    private JTextField txtEmpSalary;
    private JButton btnEmpAdd;
    private JButton btnEmpUpdate;
    private JButton btnEmpDelete;
    private JButton btnEmpGetAll;
    private JPanel panelAppTitle;
    private JPanel panelEmpActionControls;
    private JPanel panelEmpInputControls;
    private JLabel lblAppTitle;
    private JLabel lblEmpFirstName;
    private JLabel lblEmpLastName;
    private JLabel lblEmpDepartment;
    private JLabel lblEmpSalary;
    private JPanel panelTable;
    private JScrollPane scrollPane;
    private JTable tblEmpResults;
    private JButton btnExitApp;

    private final EmployeeTransactions employeeTransactions;

    public crudform() throws SQLException {

        employeeTransactions = new EmployeeTransactions();
        listAllEmployees();

        btnEmpAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                addEmployee();
            }
        });

        tblEmpResults.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                //super.mouseClicked(e);
                displayEmpClicked();
            }
        });

        btnEmpUpdate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                updateEmployee();
            }
        });

        btnEmpDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                deleteEmployee();
            }
        });

        btnEmpGetAll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    listAllEmployees();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        });

        btnExitApp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                exitApplication();
            }
        });
    }

    public JPanel getRootPanel() {
        return mainpanel;
    }

    private void addEmployee() {

        if (validations()) {
            String fName = txtEmpFirstName.getText();
            String lName = txtEmpLastName.getText();
            String department = txtEmpDepartment.getText().isEmpty() ? "" : txtEmpDepartment.getText();
            String salary = txtEmpSalary.getText().isEmpty() ? String.valueOf(0.0) : txtEmpSalary.getText();

            String strQry = "insert into emp_details (first_name, last_name, department, salary)" +
                    "values ('" + fName + "', '" + lName + "', '" + department + "', " + salary + ");";

            try {
                employeeTransactions.executeQuery(strQry);
                JOptionPane.showMessageDialog(null, "Employee Added Successfully");
                listAllEmployees();
                clearEmpInputs();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void listAllEmployees() throws SQLException {

        DefaultTableModel tableModel = new DefaultTableModel();

        tableModel.addColumn("Id");
        tableModel.addColumn("First Name");
        tableModel.addColumn("Last Name");
        tableModel.addColumn("Department");
        tableModel.addColumn("Salary");

        String strQry = "select * from emp_details;";
        ResultSet tranRS = employeeTransactions.getTransactionRecord(strQry);

        while (tranRS.next()) {
            tableModel.addRow(new Object[]{
                    tranRS.getInt(1),
                    tranRS.getString(2),
                    tranRS.getString(3),
                    tranRS.getString(4),
                    tranRS.getDouble(5),
            });
        }

        tblEmpResults.setModel(tableModel);
    }

    private void updateEmployee() {

        if (validations()) {
            int row = tblEmpResults.getSelectedRow();
            String emp_id = tblEmpResults.getModel().getValueAt(row, 0).toString();

            String fName = txtEmpFirstName.getText();
            String lName = txtEmpLastName.getText();
            String department = txtEmpDepartment.getText().isEmpty() ? "" : txtEmpDepartment.getText();
            String salary = txtEmpSalary.getText().isEmpty() ? String.valueOf(0.0) : txtEmpSalary.getText();

            String strQry = "update emp_details " +
                    "set first_name = '" + fName + "'," +
                    "last_name = '" + lName + "'," +
                    "department = '" + department + "'," +
                    "salary = " + salary + "" +
                    "where id = '" + emp_id + "';";

            try {
                employeeTransactions.executeQuery(strQry);
                JOptionPane.showMessageDialog(null, "Employee Updated Successfully");
                listAllEmployees();
                clearEmpInputs();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void deleteEmployee() {

        if (tblEmpResults.getSelectedRow() < 0) {
            JOptionPane.showMessageDialog(null, "Select any employee to delete");
        } else {
            int row = tblEmpResults.getSelectedRow();
            String emp_id = tblEmpResults.getModel().getValueAt(row, 0).toString();

            String strQry = "delete from emp_details where id = '" + emp_id + "';";

            try {
                employeeTransactions.executeQuery(strQry);
                JOptionPane.showMessageDialog(null, "Employee Deleted Successfully");
                listAllEmployees();
                clearEmpInputs();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void exitApplication() {

        JFrame frame = new JFrame("EXIT");
        if (JOptionPane.showConfirmDialog(frame, "Do you want to Exit?", "EXIT",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
            employeeTransactions.closeSQLConn();
            System.exit(0);
        }
    }

    private void clearEmpInputs() {

        txtEmpFirstName.setText("");
        txtEmpLastName.setText("");
        txtEmpDepartment.setText("");
        txtEmpSalary.setText("");
    }

    private void displayEmpClicked() {

        int i = tblEmpResults.getSelectedRow();
        TableModel model = tblEmpResults.getModel();

        txtEmpFirstName.setText(model.getValueAt(i, 1).toString());
        txtEmpLastName.setText(model.getValueAt(i, 2).toString());
        txtEmpDepartment.setText(model.getValueAt(i, 3).toString());
        txtEmpSalary.setText(model.getValueAt(i, 4).toString());
    }

    private Boolean validations() {

        if (txtEmpFirstName.getText().trim().isEmpty() && txtEmpFirstName.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "First Name cannot be empty");
            return false;
        } else if (txtEmpLastName.getText().trim().isEmpty() && txtEmpLastName.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Last Name cannot be empty");
            return false;
        } else {
            return true;
        }
    }
}
